$(document).ready(function () {
  $(".ui.accordion").accordion();
  $(".ui.dropdown").dropdown();

  //NAVBAR BOX-SHADOW CLASS ADD
  window.onscroll = function () {
    scrollFunction();
  };

  function scrollFunction() {
    if ($(window).scrollTop() > 100) {
      $("header").addClass("navbar-fixed");
    } else {
      $("header").removeClass("navbar-fixed");
    }
  }

  //HAMBURGER MENU
  $("a.target-burger").click(function (e) {
    $("div.container, nav.main-nav, a.target-burger").toggleClass("toggled");
    e.preventDefault();
  }); //target-burger-click

  // Mobile Accordion Menu
  $(".mobile-nav-dropdown").click(function (event) {
    if ($(".mobile-nav-dropdown .content").hasClass("active")) {
      $(".mobile-nav-dropdown .content").removeClass("active");
      $(".mobile-nav-dropdown .angle.icon").removeClass("up");
      $(".mobile-nav-dropdown .angle.icon").addClass("down");
    } else {
      $(this).children(".content").addClass("active");
      $(this).children(".title").children(".angle.icon").removeClass("down");
      $(this).children(".title").children(".angle.icon").addClass("up");
    }
  });

  // Smooth Scrolling
  $(".smooth-scrolling").on("click", function (event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $("html, body").animate(
        {
          scrollTop: $(hash).offset().top,
        },
        800,
        function () {
          // Add hash (#) to URL when done scrolling (default click behavior)
          window.location.hash = hash;
        }
      );
    } // End if
  });
});
