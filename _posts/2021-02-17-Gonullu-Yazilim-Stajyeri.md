---
layout: post
title: "Gönüllü Yazılım Stajyeri"
image: "/assets/img/blog/ilan/main2.jpg"
date:   2021-02-17
author: Dr. Caner Özcan
author_image: "/assets/img/teams/caner.jpg"
author_job: Kurucu, Genel Müdür
tags: [hiring]

references:
---

![İş İlanı](../assets/img/blog/ilan/yazilim.jpg)

## Genel Nitelikler ve İş Tanımı

Yapay zeka projelerinin geliştirilmesinde ve süreç iyileştirilmesinde yardımcı olabilecek, genç ve dinamik AR-GE ekibimize katılmak isteyen “Gönüllü Stajyer” pozisyonunda yer alacak takım arkadaşları aramaktayız.

### Sizden ne bekliyoruz?

- Uzaktan çalışmaya yatkın olmak ve kişisel disiplin,
- Öğrenmeye açık,
- Kurum içi iletişim becerileri sunabilme,
- Açıklık ve netlik

### Temel Nitelikler

- .Net Tabanlı Web Projeleri geliştirmiş
- MSSQL, Bootstrap, JQuery teknolojileri ile çalışmalar yapmış
- Zaman planlamasına sadık kalabilen
- Güncel teknolojileri takip eden ve kendini güncelleyebilen
- Takım çalışmasına tam olarak uyum sağlayabilecek
- Ekip arkadaşları ile edindiği yeni tecrübeleri paylaşabilen ve aktarabilen
- Dokümantasyon oluşturmada sorun yaşamayacak

**Çalışma Şekli:** Pandemi süresince uzaktan çalışma şeklinde olacaktır.

**Bize Ulaşın:** [info@simurgai.com](mailto:info@simurgai.com)
