---
layout: post
title: "Yapay Zeka Alanında Takip Edilebilecek Türkçe Kurslar"
image: "/assets/img/blog/yapay-zeka-alaninda-kurslar/main.jpg"
date:   2020-11-29
author: Dilara Özdemir
author_image: "/assets/img/teams/dilara.jpg"
author_job: Yapay Zeka Uzmanı
tags: [yapay zeka, makine ogrenimi, kurs, egitim, udemy]

references:
- Datai Team, <a href="https://dataiteam.com/" target="_blank">https://dataiteam.com/</a>
- Udemy, <a href="https://www.udemy.com/" target="_blank">https://www.udemy.com/</a>
- Kaggle, <a href="https://www.kaggle.com/" target="_blank">https://www.kaggle.com/</a>

---

Günümüzde çoğu alanda tercih edilen Yapay Zekâ hakkında takip edilebilecek ve uygulamaların öğrenilebileceği kursları inceleyelim. Öncelikle DATAI Team [1],  Udemy [2] üzerinden yayınladığı kurslar ile bizlere hem içerik olarak hem de uygulama olarak anlaşılır eğitimler sunmaktadır. 

![Yapay Zeka](../assets/img/blog/yapay-zeka-alaninda-kurslar/1.jpg)

> **UYARI:** Udemy üzerinden kursların indirimli hallerine ulaşamazsanız, https://dataiteam.com [1] sitesini de kontrol edebilirsiniz.

### **1. Python: Python Sıfırdan Uzmanlığa Programlama**

> **Kurs Linki:** https://www.udemy.com/course/python-sfrdan-uzmanlga-programlama-1

Bu kursta, Yapay Zekâ alanında en çok tercih edilen Python programlama dili anlatılmaktadır.  Önceden herhangi bir programlama bilgisine sahip olmadan bu kursu takip edenler, Python temelleri, Nesne Tabanlı Programlama, programlama hatalarının çözümü, numpy ve pandas gibi kütüphaneler ve görselleştirme için matplotlib kütüphaneleri gibi içerikleri öğrenerek Python dilinde uzman olabilmektedir. Eğer Python konusunda yeterince bilgili olduğunu hissediyorsanız bu kursu atlayıp ikinci kurs olan Veri Bilimi kursuna geçebilirsiniz.

### **2. Data Science ve Python: Sıfırdan Uzmanlığa Veri Bilimi**

> **Kurs Linki:** https://www.udemy.com/course/data-science-sfrdan-uzmanlga-veri-bilimi-2

Veri bilimi alanında ya da Python dilinde uzmanlaşmak isteyenler bu kursu takip edebilir. Tabii ki veri bilimi, Yapay Zekâ alanının yapıtaşlarından biri olduğu için bu kursu anlaşılır notlar alarak ve uygulamaları eksiksiz yaparak takip etmenizi tavsiye ederiz. Bu kursu diğer veri bilimi kurslarından ayıran en önemli özellik, yapay zeka için gerekli olan tüm bilgileri temelden alarak vermesi ve veri işlemeyi Kaggle [3] gibi bir platform üzerinden anlatılmasıdır. 

### **3. Data Visualization: A'dan Z'ye Veri Görselleştirme**

> **Kurs Linki:** https://www.udemy.com/course/data-visualization-adan-zye-veri-gorsellestirme-3

Bu kursu alırken uygulamaların anlaşılabilmesi adına Python temellerini bilmeniz gerekmektedir. Kursun içeriğinde en çok kullanılan görselleştirme kütüphaneleri olan **Seaborn** ve **Plotly** anlatılmaktadır. Bir çok görselleştirme tekniği üzerinden çeşitli uygulamalar ile verinin daha anlaşılır bir hale nasıl getirebileceğini göstermektedir. Aynı zamanda nadir kullanılan görselleştirme araçları ve kütüphaneleri de kurs içerisinde bulunmaktadır.

### **4. Machine Learning ve Python: A'dan Z'ye Makine Öğrenmesi**

> **Kurs Linki:** https://www.udemy.com/course/machine-learning-ve-python-adan-zye-makine-ogrenmesi-4

Yapay Zekâ temellerinden olan Makine Öğrenmesi, bu kursta hem kod yazımı hem de kodun mantığı ile anlatılmaktadır. İçerik olarak **gözetimli öğrenme, gözetimsiz öğrenme, Doğal Dil İşleme (NLP), Temel  Bileşenler Analizi (PCA), Model seçimi ve Öneri Sistemleri** konu başlıklarına sahiptir.

### **5. Deep Learning ve Python: A'dan Z'ye Derin Öğrenme**

> **Kurs Linki:** https://www.udemy.com/course/deep-learning-ve-python-adan-zye-derin-ogrenme-5

Yapay Zekâ yolculuğumuzun beşinci adımı olan Derin Öğrenme kursu **Logistic Regression, Artificial Neural Network (ANN), Convolutional Neural Network (CNN), Recurrent Neural Network (RNN)** konu başlıklarını içerir. Aynı zamanda bu kursun uygulamaları Anaconda Jupyter Notebook üzerinden yapıldığı için farklı bir ortamda da deneyim kazanmanızı sağlar. 

Derin Öğrenme ve Makine Öğrenmesi alanlarında kendinizi daha ileri seviyelere taşımak istiyorsanız aşağıdaki kursları da gözden geçirebilirsiniz.

1. **Deep Learning ve Python: İleri Seviye Derin Öğrenme** 

**Kurs Linki:** https://www.udemy.com/course/deep-learning-ve-python-ileri-seviye-derin-ogrenme-52

2. **Python ile Makine Öğrenmesi & Yapay Zeka Projeleri**

**Kurs Linki:** https://www.udemy.com/course/python-ile-makine-ogrenmesi-yapay-zeka-projeleri-52

3. **Python ve Derin Öğrenme: Pytorch ile Derin Öğrenme**

**Kurs Linki:** https://www.udemy.com/course/python-ve-derin-ogrenme-pytorch-ile-derin-ogrenme-53


### **6. İstatistik & Python: A'dan Z'ye Temel İstatistik Bilimi**

> **Kurs Linki:** https://www.udemy.com/course/istatistik-python-adan-zye-temel-istatistik-bilimi-6

İstatistik, veriyi toplama ve anlama hatta veriyi kontrol edebilmedir. Elimizdeki verinin analizinde istatistik bilimi çok büyük bir rol oynar. Bu bağlamda istatistik bilimi kursu ile bir çok kavramı öğrenerek gerçek hayat problemlerine uygulayacaksınız. Konu başlıkları olarak **veri, olasılık, olasılıksal dağılımlar, istatistik, ANOVA ve Chi-Square Analysis** bulunmaktadır.


### **7. Python ile Yapay Zekâ: A'dan Z'ye Reinforcement Learning**

> **Kurs Linki:** https://www.udemy.com/course/python-ile-yapay-zeka-adan-zye-reinforcement-learning

Pekiştirmeli Öğrenme algoritmalarının ardında yatan matematiği, mantığı, teoriyi anlayarak bu algoritmaların Python ile sıfırdan nasıl kodlandığını ve Python ile oyun ortamı oluşturmayı öğrenerek uygulayabileceksiniz. Kursa başlamadan önce Yapay Sinir Ağları (ANN), Python ile Nesne Tabanlı Programlama, Evrişimsel Sinir Ağları (CNN) konularına hakim olmak gerekmektedir. Bu kurs, **Q-Learning, Deep Q-Learning, Environment Design (ortam tasarımı), Deep Convolutional Q-Learning** konu başlıklarını içerir.

![Yapay Zeka](../assets/img/blog/yapay-zeka-alaninda-kurslar/2.jpg)

Umarım hazırlanan bu “7 Adımda Yapay Zekâ Yolculuğu” serisi sizlere faydalı olur. Geriye sadece sıkı çalışmak ve bol bol uygulama yaparak pekiştirmek kalıyor. Bir sonraki yazımızda sizlere kurslar hakkında daha çok bilgi vereceğiz, takipte kalabilirsiniz.
