---
layout: post
title: "Sağlık Sektöründe ve Diş Hekimliğinde Yapay Zeka"
image: "/assets/img/blog/saglikta-yapay-zeka/main.jpg"
date:   2020-11-08
author: Buse Yaren Tekin
author_image: "/assets/img/teams/buse.jpg"
author_job: Takım Lideri
tags: [yapay zeka, saglikta yapay zeka, dis hekimligi, saglik]

references:
- From Wikipedia, The free encyclopedia, September 2020, Artificial Intelligence in Healthcare. Vikipedi, Özgür Ansiklopedi, Eylül 2020, Sağlık Hizmetlerinde Yapay Zeka.
- <a href="https://www.mathematica.org/commentary/ethics-and-artificial-intelligence-in-health-care-the-pivot-point" target="_blank">https://www.mathematica.org/commentary/ethics-and-artificial-intelligence-in-health-care-the-pivot-point</a> adresinden alınmıştır.
- Medicana, Sağlık Rehberi, Da Vinci Robotik Cerrahi Sistemi Nedir ve Ameliyatlarda Nasıl Kullanılır?, <a href="https://www.medicana.com.tr/saglik-rehberi-detay/11846/da-vinci-robotik-cerrahi-sistemi-nedir-ve-ameliyatlarda-nasil-kullanilir#da-vinci-robotik-cerrahi-sistemi-hakkinda-onemli-bilgiler" target="_blank">https://www.medicana.com.tr/saglik-rehberi-detay/11846/da-vinci-robotik-cerrahi-sistemi-nedir-ve-ameliyatlarda-nasil-kullanilir#da-vinci-robotik-cerrahi-sistemi-hakkinda-onemli-bilgiler</a>.
- From Wikipedia, The free encyclopedia, October 2020, da Vinci Surgical System.
- <a href="https://www.leetrevinodental.com/growing-teeth-the-amazing-new-technology-in-dentistry/" target="_blank">https://www.leetrevinodental.com/growing-teeth-the-amazing-new-technology-in-dentistry/</a> adresinden alınmıştır.
- <a href="https://awesomeopensource.com/project/clemkoa/tooth-detection" target="_blank">https://awesomeopensource.com/project/clemkoa/tooth-detection</a> adresinden alınmıştır.
- Ayşe Betül Oktay, Tübitak, Diş Görüntülerinin Biyometrik Olarak Kullanılarak Otomatik Kimlik Belirlenmesi, Ocak 2016.
- Changgyun Kim, Donghyun Kim, HoGul Jeong, Suk-Ja Yoon, Sekyoung Youm, Automatic Tooth Detection and Numbering Using a Combination of a CNN and Heuristic Algorithm, Appl. Sci. 2020, 10(16), 5624.

---

Sağlık hizmetlerinde yapay zekâ, karmaşık tıbbi ve sağlık verilerinin analizinde, yorumlanmasında ve anlaşılmasında insan bilimini taklit etmek için karmaşık algoritmaların veya diğer bir ifade ile yapay zekanın kullanılmasıdır [1]. Spesifik olarak yapay zekâ, bilgisayar algoritmalarının doğrudan insan desteği olmadan sonuçlara yaklaşma yeteneğidir. Yapay zekâ kullanımı bireysel olarak karar verme tekniklerine göre birçok avantajı beraberinde getirir. Karar verme mekanizması söz konusu olduğunda bireyin yorgunluk ve dikkatsizlik gibi subjektif gerçekler ile gözünden kaçabilecek ihtimaller yapay zekâ stratejisi sayesinde ortadan kaldırılmaktadır. Günümüzde özellikle COVID-19 gibi pandemi süreçli dönemlerde daha az doktor-hasta temasının öngörülmesi hedefinde yapay zekâ sağlık sektörüne de aktif olarak hızlı bir şekilde giriş yapmıştır. Bunların yanı sıra sağlık çalışanlarının hastaları için gerçekleştirdiği tedavilerde düzenli olarak takip edememe sorunu da yaşanmaktadır. Ek olarak sağlık hizmetleri alanında kullanılan yapay zekâ uygulamaları sonucu hasta yatağı kapasitesi, muayene süresi optimize edilerek minimuma indirilmiştir.

![Sağlıkta Yapay Zeka Teknoloji](../assets/img/blog/saglikta-yapay-zeka/1.jpg)
<center>*Sağlıkta Yapay Zeka Teknoloji [2]*</center>

Günümüzde birçok dijital sağlık uygulamalarına denk gelmişsinizdir. Tabi yalnızca sağlık sektöründe değil aklımıza gelebilecek her alanda makine öğrenimi kullanılmaya başlanmıştır. Sizce sağlık sektörüne yapay zekâ teknolojileri ilk adımları ne zaman atmaya başlamış olabilir? Araştırmalarımız sonucunda yapay zekâ ve sağlık sektörünün birlikte çalışmaya başladığı zamanlarda ilk sistemlerden birisi Da Vinci Robotik Cerrahi Sistemi olmaktadır. Bu sistem ile Cerrah, konsol başında oturarak ameliyatı gerçekleştirdiği için daha az yorulur. Böylece, uzun süren cerrahilerde doktor ameliyata daha iyi odaklanabilir ve herhangi bir konsantrasyon kaybı yaşanmaz[3]. Bu konu ile ilgili sizleri daha çok aydınlatmak adına bir görsel ile incelemeye devam edelim. Görsel dikkatle incelendiğinde bu yapay zekalı cerrahi sistem aynı anda birden çok ameliyata tanıklık edebilir bunun yanı sıra sinirler ve damarlar daha ayrıntılı görüntülendiği için bu dokuların zarar görme riski daha azdır.

![Da Vinci Cerrahi Sistem](../assets/img/blog/saglikta-yapay-zeka/2.jpg)
<center>*Da Vinci Cerrahi Sistem [4]*</center>

Da Vinci Cerrahi Sistem ile ürolojik cerrahi, obezite cerrahisi, jinekolojik cerrahi, genel cerrahi, kulak burun boğaz (KBB) ve baş boyun cerrahisi gibi birçok alanda hizmet vermektedir. Dijital çağda hız kesmeden yapay zekâ çalışmalarına devam etmekteyiz. Bizler de bu alanın bu ekibin bir parçası sayılırız. Ne demiş Y. LeCun, “Bizi insan yapan zekamızdır ve yapay zekâ bu kalitenin bir uzantısıdır.” Bunu dikkate alarak sektöre yön vermek başlıca hedeflerden olmalıdır.

### Diş Hekimliğinde Yapay Zeka

![Diş Hekimliğinde Yapay Zekâ Teknolojisi](../assets/img/blog/saglikta-yapay-zeka/3.jpg)
<center>*Diş Hekimliğinde Yapay Zekâ Teknolojisi [5]*</center>

Sağlıkta yapay zeka söz konusu olduğunda, elde edilen büyük verinin kullanılabilirliği ölçülmekte ve özellikle hasta teşhisi için ön plana çıkması sağlanmaktadır. Genellikle MRI röntgen görüntülerinde yer verilen belirgin bölgeler makine tarafından tespit edilmesi segmentasyon gerçekleştirilerek sağlanmaktadır. Yapay zeka uygulamaları ile insan kaynağı ihtiyacı ve bunun getirdiği maliyet azaltmaktadır. Doktor açığının yaşandığı sağlık merkezlerinde bu sistemler ile hasta sağlığı, maliyet ve zaman açısından avantaj sağlanmaktadır. Böylelikle yapay zeka daha çok sektörlerde yer edinmeye başlamıştır. Konu diş hekimliğine geldiğinde ise, radyologlar ve diş hekimleri tarafından gerçekleştirilen zahmetli ve uzun süre alabilen diş tespiti işlemi yapay zeka ile çok kısa süreye indirgenmiştir.

![Dişlerin ve Diş Problemlerinin Tespit Edilmesi](../assets/img/blog/saglikta-yapay-zeka/4.jpg)
<center>*Dişlerin ve Diş Problemlerinin Tespit Edilmesi [6]*</center>

Yukarıda yer verdiğimiz görselde radyografik bir diş görüntüsü üzerinde diş tespiti yapılmaktadır. İmplant, endodontik ve diş restorasyonları bir makine tarafından belirlenmektedir. Bu sayede dişlerin tespitini uzman bir radyolog veya diş hekimi yapmak zorunda kalmamaktadır. Zamandan tasarruf sağlanarak muayene süreleri indirgenmektedir. Günümüzde diş hekimliği alanında kanal kök morfolojisi, kanal segmentasyonu, diş numaralandırma gibi birçok farklı alanda yapay öğrenme sağlanarak planlama tedavileri sağlanmaktadır. Literatürde yer alan birçok makalede ve çalışmada panoramik röntgen görüntüleri üzerinde tespit yapılmaktadır. Panoramik görüntü; çenelerin, tüm dişlerin, çene ve dişlerdeki birçok rahatsızlığın tek bir filmde görülmesini sağlayan röntgen filmleridir. Panoramik dental görüntüler çekilirken kişi plastik bir çubuğu ısırmakta ve bu da ağzın açık kalmasını sağlamaktadır çünkü ağız kapalıyken alt ve üst dişler birbirlerinin görünmesini engellemektedir. Kişi hareketsiz beklerken,bir X-ışını üreticisi ve alıcısı kafanın etrafında dönmekte ve ağzı kapsayan X-ışını görüntülerini çekmektedir. Bu çekilen görüntüler birbirleriyle birleştirilerek panoramik görüntü elde edilmektedir [7]. Aşağıdaki şekilde bir panoramik dental görüntü ve diş etiketleri gösterilmiştir. Sağ üst küçük azılar görüntü oluşturulurken meydana gelen bozulmadan dolayı birbirleriyle üst üste duruyormuş gibi gözükmektedir.

![Panoramik Görüntü Örneği](../assets/img/blog/saglikta-yapay-zeka/5.jpg)
<center>*Panoramik Görüntü Örneği [7]*</center>

![](../assets/img/blog/saglikta-yapay-zeka/6.jpg)
<center>*Her satırda bir kişiye ait panoramik dental görüntünde tespit edilen aday dişler ve sonunda tespit edilen kesin diş konumları. (a), (c) ve (e)’de aday diş olarak bulunan pencerelerin merkezleri gösterilmiştir. Büyük azı dişler kırmızı, küçük azı dişler mavi ve ön dişler yeşil renk gösterilmiştir. (b), (d), ve (f) bulunan dişlerin merkezleri gösterilmiştir [6].*</center>

Peki sizce neden böyle bir ihtiyaç aranmıştır? Daha önce de vurguladığımız gibi doktor açığının bulunduğu sağlık merkezlerinde yapay zeka sistemleri ile hasta sağlığı, maliyet ve zaman açısından avantaj sağlanmak istenmiştir. Bunların yanı sıra tartışmasız küresel olarak en sık karşılaşılan sorunlardan bir tanesi ‘Yapay zekâ, sağlık çalışanlarının işlerini ellerinden alacak mı?’ gibi görünüyor. Bu soru her ne kadar insanların kafasını karıştırsa da gelecekte görülecektir ki yapay zeka insanların işlerini elinden almak değil yardımcı olmak için bizlerle birliktedir. Yardımcı oldukları işler hızlanacak ve belki de bambaşka mesleklere yer bulunacaktır. Dediğim gibi önemli olan tasarruf ve optimizasyondur. Belki de insanoğlunun subjektif yaklaşımları yerine makinenin objektif ve tekdüze verdiği tedavi sonuçları ile özellikle de sağlık alanındaki yanılsamalar kaldırılacaktır.