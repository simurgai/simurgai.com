---
layout: post
title: "Uluslararası Yayınlarımız"
image: "/assets/img/blog/yayin/main.jpg"
date:   2021-06-08
author: Dr. Caner Özcan
author_image: "/assets/img/teams/caner.jpg"
author_job: Kurucu, Genel Müdür
tags: [academy,international,yapay zeka,dentiassist,deep learning, congress, healthcare]

references:
---

SimurgAI uluslararası literatürde! Diş hekimliği öğrencilerinin eğitimi için geliştirdiğimiz yapay zeka destekli otomatik diş segmentasyonu modülümüz uluslararası dergide yayına kabul aldı. 

* **Bildiri kitabı:** <a href="https://aita.bakircay.edu.tr/Yuklenenler/AITA/A21_02_001.pdf/" target="_blank">https://aita.bakircay.edu.tr/Yuklenenler/AITA/A21_02_001.pdf/</a>
* **Makale kitabı:** <a href="https://aita.bakircay.edu.tr/Yuklenenler/AITA/A21_02_002.pdf/" target="_blank">https://aita.bakircay.edu.tr/Yuklenenler/AITA/A21_02_002.pdf/</a>

<img src="../assets/img/blog/yayin/tam.png" alt="drawing" style="max-height: 100%" />
