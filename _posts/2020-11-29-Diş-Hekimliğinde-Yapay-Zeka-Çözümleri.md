---
layout: post
title: "Diş Hekimliğinde Yapay Zeka Çözümleri"
image: "/assets/img/blog/dis-hekimliginde-yapay-zeka-cozumleri/main.jpg"
date:   2020-11-29
author: Betül Dolapçı
author_image: "/assets/img/teams/betul.jpg"
author_job: Görüntü İşleme ve Makine Öğrenmesi Uzmanı
tags: [yapay zeka, diş hekimliğinde yapay zeka, dental görüntü]


references:
- <a href="https://www.denti.ai" target="_blank">Denti AI</a>
- <a href="https://diagnocat.com" target="_blank">Diagnocat</a>
- <a href="http://www.kapanu.com" target="_blank">Kapanu</a>
- <a href="https://dentalxr.ai" target="_blank">DentalXr.ai</a>
- <a href="https://www.dentaid.org" target="_blank">DentAid</a>
- <a href="https://www.dentem.co/" target="_blank">Dentem</a>

---

Yapay Zeka, bugün hemen hemen tüm alanlarda kullanılarak insanoğlunun çoğu iş yükünü azaltarak hayatımızda çok önemli bir yer edinmiştir. Özellikle sağlık sektöründe kullanılarak çok önemli işlere imza attığımız yapay zekayı, diş hekimliği alanında kullananlar bu alanı nasıl değerlendirmişler bir göz atalım.

### **1. Denti AI**
![](../assets/img/blog/dis-hekimliginde-yapay-zeka-cozumleri/1.png)

**Denti.AI**, diş hekimlerinin yapay zeka kullanarak X ışını görüntülerini yorumlanması ve %30 daha fazla potansiyel patolojiyi bulmasına yardımcı olmaktadır. Kronlar, ekstraksiyonlar, köprüler ve implantlar gibi önceki tedavinin saptanmasını içeren çizelge oluşturulmaktadır. Apikal periodontitis ve çürük gibi patolojiler için ilgi alanlarının otomatik tespiti yapılmaktadır. Analiz, inceleme ve onay, hızlı bir şekilde bir PDF belgesinde ayrıntılı bir sunum haline dönüştürülür.

### **2. Diagnocat**

![](../assets/img/blog/dis-hekimliginde-yapay-zeka-cozumleri/2.jpeg)

**Diagnocat**, sinir ağı, maksillofasiyal anatomiyi, koşulları ve anormallikleri 3 dakika içinde tanır. Ölçümler ve tanı için son derece bilgilendirici değere sahip panoramik ve kesitsel tomografi görüntüleri sağlar. İlgili dişlerin açıklamaları ve görüntülerini içeren radyoloji raporu çıkarılır.

### **3. Kapanu**

![](../assets/img/blog/dis-hekimliginde-yapay-zeka-cozumleri/3.jpeg)

ETH Zürih Görsel Hesaplama Enstitüsü / Bilgisayar Grafikleri Laboratuvarından bir yan kuruluş olan KAPANU, görsel hesaplama teknolojileri, bilgisayarla görme, bilgisayar grafikleri ve makine öğrenimi konularında ileri araştırma ve mühendislik alanlarında çalışmaktadır. Gerçek renkte etkileşimli içerik ve görselleştirme oluşturma, gerçek zamanlı 3D uygulamaları ile dişlerin yeni görünümünü sizlere sunar. 

### **4. DentalXR AI**

![](../assets/img/blog/dis-hekimliginde-yapay-zeka-cozumleri/4.png)

Yapay Zeka Teknolojisi kullanılarak, diş hekimlerinin dental röntgen görüntülerinde patolojileri ve restorasyonları tespit etmesine yardımcı olan bir karar destek platformudur. Röntgen görüntülerinin değerlendirilmesi ve ardından mevcut restorasyonların ve yeni patolojilerin sıkıcı dokümantasyonu için zaman kazandırır. "ikinci görüş" sağlayarak hastalarla daha iyi ve daha güvenilir ilişkiler geliştirir.

### **5. DentAid**

![](../assets/img/blog/dis-hekimliginde-yapay-zeka-cozumleri/5.png)

**DentAid**, ağız hastalıklarını daha iyi teşhis etmek ve bir diş hekiminin daha doğru ve daha hızlı teşhis konmasına yardımcı olmak için derin bir öğrenme çözümüdür.
Diş hekimliğindeki radyografiler, diş hekimlerinin bir dizi ağız ve çene-yüz hastalıklarını teşhis etmesine ve bu teşhise dayalı olarak uygun tedavileri ve müdahaleleri önermesine yardımcı olabilir. 
Belirli oral ve maksillofasiyal hastalıkları doğru bir şekilde tespit edebilen sınıflandırıcılar oluşturmak için Evrişimsel Sinir Ağları ile Derin Öğrenme tekniklerini kullanmayı amaç edinmişlerdir. Hastalıklar arasında çürük, kist ve tümörlerin yanı sıra yanlış kanal tedavilerinin teşhisi yer alır. Ayrıca, test OPG' sini diş numaralandırma, diş segmentasyonu, cinsiyet tespiti vb. ile açıklamak için görüntü işleme teknikleri de kullanmışlardır.
### **6. Dentem**

![](../assets/img/blog/dis-hekimliginde-yapay-zeka-cozumleri/6.jpg)

DX-Vision ürünü Dentem tarafından yaratılan bir üründür ve dental yazılımda geleceği getirmek için gelişmektedir. Dx-Vision, hastaların diş röntgenindeki sorunlara çözüm üretmektedir. Dentem tüm dijital röntgenleri saklar ve uygun görülen her an DX-Vision ile tarayabilir. Uygulama ile randevu hatırlatıcıları, insanları programın ilerisinde tutmak ve kaçırılan randevuları azaltmak için senkronize edilmektedir. Dentem, Corp. Innovation Lab tarafından geliştirilen makine öğrenimi algoritması geliştirilmeye devam edilmektedir. 

Modelin ikinci versiyonu, gelişiminin erken aşamalarında ve içinde bazı mevcut değişiklikler var olduğunu belirtmektedir. En önemli yeni özelliğin, sınırlayıcı kutular yerine maskeler veya çokgenler kullanılması belirtilmektedir. Bu, daha fazla hassasiyete ve farklı sorunların daha iyi bir genel tespitine izin verecektir. Hala geliştirme aşamasında olduğu için bu yeni modelin özelliklerine yeni etiketler eklenecektir. Diğer bir çıkış özelliği, üçüncü şahıslar tarafından yaygın olarak kullanılabilecek kendi kendine hizmet veren bir API'dir. Bu, geliştiricilere daha iyi erişim sağlayacak ve binlerce hastanın diş sağlığını iyileştirmeyi hedef tutmaktadır.

