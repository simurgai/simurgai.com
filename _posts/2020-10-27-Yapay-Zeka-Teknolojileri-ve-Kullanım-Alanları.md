---
layout: post
title: "Yapay Zeka Teknolojileri ve Kullanım Alanları"
image: "/assets/img/blog/yapay-zeka-teknolojileri/main.jpg"
date:   2020-10-27
author: Elif Meşeci
author_image: "/assets/img/teams/elif.jpg"
author_job: Yapay Zeka Uzmanı
tags: [yapay zeka, yapay zeka kullanim alanlari]


references:
- <a href="https://assistant.google.com/" target="_blank">Google Asistan</a>
- <a href="https://medium.com/@ayyucekizrak/yapay-zeka-kullan%C4%B1m-alanlar%C4%B1-ve-uygulamalar%C4%B1na-derinlemesine-bir-bak%C4%B1%C5%9F-d0fecaf7f61b" target="_blank">https://medium.com/@ayyucekizrak/yapay-zeka-kullan%C4%B1m-alanlar%C4%B1-ve-uygulamalar%C4%B1na-derinlemesine-bir-bak%C4%B1%C5%9F-d0fecaf7f61b</a> adresinden alınmıştır.
- <a href="https://www.yapayzekatr.com/2020/01/06/yapay-zeka-ve-kullanim-alanlari/" target="_blank">https://www.yapayzekatr.com/2020/01/06/yapay-zeka-ve-kullanim-alanlari/</a> adresinden alınmıştır.
- <a href="https://robopress.robotsandpencils.com/what-is-artificial-intelligence-and-why-should-i-care-151b2fa8d3ab?gi=76c1880786" target="_blank">https://robopress.robotsandpencils.com/what-is-artificial-intelligence-and-why-should-i-care-151b2fa8d3ab?gi=76c1880786</a> adresinden alınmıştır.
- <a href="https://techburst.io/what-is-artificial-intelligence-ai-what-is-machine-learning-ml-1a7ac5247a10?gi=5e33820e6634" target="_blank">https://techburst.io/what-is-artificial-intelligence-ai-what-is-machine-learning-ml-1a7ac5247a10?gi=5e33820e6634</a> adresinden alınmıştır.
- <a href="https://medium.com/hackernoon/so-you-think-you-know-what-is-artificial-intelligence-6928db640c42" target="_blank">https://medium.com/hackernoon/so-you-think-you-know-what-is-artificial-intelligence-6928db640c42</a> adresinden alınmıştır.
- <a href="https://pranavathiyani.medium.com/what-is-artificial-intelligence-ai-ad5ba87b55dd" target="_blank">https://pranavathiyani.medium.com/what-is-artificial-intelligence-ai-ad5ba87b55dd</a> adresinden alınmıştır.

---

### Yapay Zeka Nedir?

Yapay Zekâ, insan beyninin **"öğrenme, hareket edebilme, kontrol edebilme, planlama, algılama ve manipüle etme"** gibi becerilerinin bir makine içerisinde tasarlanması olarak düşünülebilir. Bu bağlamda yapay zekânın sıradan bir yazılımdan ayrılmasının en önemli sebebi; bilinen en zeki varlık olan insan zekâsını taklit etmesinden geçmektedir diyebiliriz.
Yapay Zekâ’nın babası John McCarthy' e göre yapay zekâ, bilim, akıllı makineler -özellikle akıllı bilgisayar programları- yapma mühendisliğidir. Yapay Zekâ denildiğinde akla ilk gelen şey Matrix, Terminatör, Ex machina gibi robotlar ya da düşünebilen makineler oluyor. Bu yapay zekânın uygun ama belirsiz bir anlayışıdır. Bu yazıda yapay zekânın ne olduğunu ve zamanla nasıl değiştiğini aynı zamanda kullanım alanlarını inceleyeceğiz.

![](../assets/img/blog/yapay-zeka-teknolojileri/1.jpg)

Yapay Zekâ aslında yeni bir şey değil. Bu terim 60 yıldan fazla süredir var. Aslında, Yapay Zekâ, 1956'da Dartmouth' da matematik profesörü olan McCarthy tarafından bir araştırma makalesinde ortaya çıkarıldı.

*"Every aspect of learning or any other feature of intelligence can in principle be so precisely described that a machine can be made to simulate it."*
*"Öğrenmenin her yönü veya zekânın başka herhangi bir özelliği prensipte o kadar kesin bir şekilde tanımlanabilir ki, onu simüle etmek için bir makine yapılabilir."*

#### Öyleyse konu neden bu kadar eski olduğu halde şimdi popüler?

Bilgisayar bilimcileri yarım yüzyılı aşkın süredir yapay zekâyı keşfederken, yalnızca son birkaç yılda manşetlere taşınıyor ve önemli miktarda sermayenin ilgisini çekiyor.

- Bulut bilişim gelişti. Sadece erişimi zor olmakla kalmayıp aynı zamanda maliyetli olan şirket içinde depolanan verilere sahip olmak yerine, artık bulutta çok daha düşük bir maliyetle depolanan verilere sahibiz.
- İnternet gelişti. İnsanların nasıl bilgi aradıkları ve sorular sorduğuna dair bilginin yanı sıra zengin bilgiye de erişimimiz var.
- Donanımda büyük gelişmeler oldu. Eskiden süper bir bilgisayar gerektiren algoritmalar artık cep telefonunuzda çalışabilir.
- Sahada gelişmeler oldu. Yapay zekâ alanında okuyan ve çalışan zeki insanlar denemeye, öğrenmeye ve ilerlemeye devam ettiler.

Her şeyden önce, dünya daha bağlantılı hale geldikçe, yapay zekâ ilerlemelerinin gelişmesine izin veren büyük miktarda veri oluşturuyoruz. Şunu bir düşünün: Bugün tüm verilerin % 90'ından fazlası son dört yılda oluşturulmuş. Bu oluşturulan veriler ışığında Yapay Zekâ teknolojileri de hayatımıza girmiş bulunmaktadır ve insan hayatını kolaylaştırmaktan başlayarak hataları en aza indirgemeye kadar birçok fonksiyona sahip olduğunu bize kanıtlayarak günümüzde çoğu alanda karşımıza çıkmaktadır.

### **Günümüzde Yapay Zekâ Teknolojilerinin Kullanım Alanları**

#### **1. Sağlık**

![](../assets/img/blog/yapay-zeka-teknolojileri/2.jpg)

Yapay Zekâ, sağlık alanında birçok uygulama ile karşımıza çıkmaktadır. Örnek olarak hasta verilerinin analizi yapılarak yapay zeka destekli teşhisler ile daha doğru sonuçlar elde edilebiliyor.Yapay zekâ sağlık konusu olduğunda kuşkusuz tıbbi görüntüleme alanında da yer edinmektedir. Bu noktada görüntülerin daha iyi analiz edilmesi için yüksek görüntü işleme özelliklerine sahip teşhis platformları kullanılmaktadır. Bu sayede hata oranı düşürülüp görüntülerdeki hastalıkları daha iyi tespit edilebilmektedir ve teşhis noktasında bize oldukça kolaylık sağlamaktadır.

#### **2. Eğitim**

![](../assets/img/blog/yapay-zeka-teknolojileri/3.jpg)

Eğitim alanında yapay zekâ; planlama, değerlendirme, kişileştirme ve otomatikleştirme gibi çokça kolaylık sağlamaktadır. Yapay zekâ teknolojileri, öğrencilerin eksikliklerini analiz edip bireye uygun bir program tasarlayabilmektedir. Öğrenci verileri kullanılarak tasarlanan bu program sayesinde değerlendirme ve performansı arttırmaya yönelik öneriler sağlaması da bir avantaj sağlamaktadır. Öğretmenler açısından bakıldığında da görevlerin otomatikleştirilmesi gibi uygulamalar ile iş yükü azaltılmaktadır.

#### **3. Siber Güvenlik**

![](../assets/img/blog/yapay-zeka-teknolojileri/4.jpg)

Siber Güvenlik alanında analistlerin yaklaşan saldırıları tahmin etmelerine ve izleyebilmelerine yardımcı olmasından dolayı yapay zekâ teknolojileri, birçok önlemi de beraberinde sağlamaktadır. Çok katmanlı gelişmiş şifreleme teknikleri ve kısa sürede yapılan değişiklikler sayesinde haberleşmeyi de güvenli bir hale getirebilmektedir.

#### **4. Danışmanlık / Öneri Sistemleri**

![](../assets/img/blog/yapay-zeka-teknolojileri/5.jpg)

Yapay Zekâ teknolojileri, sağladığı asistanlık hizmetleri ile birçok yerde karşımıza çıkmaktadır. Sohbet ara yüzleri, doğal dil işleme, ses analizleri ve daha fazla hizmeti ile yine yardımcı birçok araç sunmaktadır. Örneğin, markalaşmış bir film uygulamasında bir filmi izlediğimizi düşünelim. Daha sonra, bu siteye girdiğimiz zaman izlediğimiz filme benzer filmler karşımıza çıkmaktadır. Bunu sağlayan yapay zekâ teknolojisi, öneri sistemi olarak adlandırılmaktadır. Asistanlık hizmetine örnek vermek gerekirse, Google’ın bize sunduğu Google Asistan[1] görevleri yönetebilir, günümüzü planlayabilir, önerilerde bulunabilir ve sorularımıza yanıtlar verebilir.

#### **5. Hukuk**

![](../assets/img/blog/yapay-zeka-teknolojileri/6.jpg)

Hukuk alanında en çok yazılı belgenin incelenmesinde kullanılan yapay zekâ teknolojileri, aynı zamanda da dava veya vaka inceleme için de kullanılmaktadır. Yazılı belgenin incelenmesi yapılırken kullanılan uygulamalara sözleşme taslakları oluşturma ve yönetimini örnek olarak verebiliriz. Sözleşmelerin çoğu belli kalıplar içermektedir. Bu sözleşmelerin taslakları hazırlanırken doğal dil işlemeden yararlanan bir yapay zekâ teknolojisi karşımıza çıkmaktadır. Dava veya vaka inceleme örneği içinde karar tavsiye sistemlerini ele alabiliriz. Daha önceki uygulama örneği gibi bu sistemde de doğal dil işleme metotları kullanılmaktadır. Karar tavsiye sistemleri uygulanırken, daha önceki benzer vakalarda verilen kararlara bakılmaktadır ve bu kararlara bağlı olarak öneriler, yapay zekâ teknolojisi tarafından bize sunulmaktadır.

#### **6. Otomotiv**

![](../assets/img/blog/yapay-zeka-teknolojileri/7.jpg)

Çevresel faktörler göz önünde bulundurularak araç içerisindekilerin konforu ve güvenliği kapsamında geliştirilen yapay zekâ teknolojisi sürücüsüz araçlar alt başlığında karşımıza çıkmaktadır. Aynı zamanda sürücüsüz araçlar sanayiden savunmaya kadar birçok alanda kullanılmaktadır ve yapay zekâ gücünden yararlanarak verimliliği oldukça arttırmaktadır. Görüş sistemleri, görüntü analizi, görüntü işleme ve nesne tespiti ile sürücüsüz araçların hedefe ulaşması adımında kullanılmaktadır. Sürüş asistanlarını da otomotiv alanında yapay zekâ teknolojisinin kullanımına bir örnek olarak verebiliriz. Sürücü deneyimini geliştirmek ve iyileştirmek için bu teknolojiden yararlanılmaktadır.

Günümüzde AI modelleri, çözmeye çalıştıkları görevle ilgili belirli verilere dayanmaktadır. Makine bir alandan kavramları hemen soyutlayıp diğerine uygulayamaz - Tesla'yı çalıştıran sistem de Go oynayamaz.

Açıkça görülüyor ki, insanların da araba sürmeden veya Go oynamadan önce kuralları öğrenmeleri gerekiyor. Fark bilişsel olarak, insanlar hayatlarının farklı bölümlerinden öğrendiklerini yeni görevlere uygulayabilirler. Fakat yapay zekâda bu farklı bir boyut...
Facebook'un AI araştırma direktörü Yann LeCun' un açıkladığı gibi:

*"Most of human and animal learning is unsupervised learning. If intelligence was a cake, unsupervised learning would be the cake, supervised learning would be the icing on the cake, and reinforcement learning would be the cherry on the cake. We know how to make the icing and the cherry, but we don’t know how to make the cake. We need to solve the unsupervised learning problem before we can even think of getting to true AI."*
*"İnsan ve hayvan öğreniminin çoğu denetimsiz öğrenmedir. Zekâ bir pasta olsaydı, denetimsiz öğrenme kek olurdu, denetimli öğrenme pastanın üzerine krema olurdu ve pekiştirmeli öğrenme pastadaki kiraz olurdu. Krema ve vişne yapmayı biliyoruz, ancak pastayı nasıl yapacağımızı bilmiyoruz. Gerçek yapay zekâya ulaşmayı düşünmeden önce denetimsiz öğrenme problemini çözmemiz gerekiyor."*

##### Peki neden yapay zekâyı önemsemeliyiz?

İnternetin ortaya çıkışının, insanların iş yapma ve dünyayla etkileşim kurma biçimiyle ilgili her şeyi değiştirmesi gibi, AI teknolojilerinin de benzer bir etkiye sahip olduğunu görüyoruz. Yapay Zekâ çağında kesinlikle geride kalmak istemezsiniz.