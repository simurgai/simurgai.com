---
layout: post
title: "DentiAssist Acıbadem Üniversitesi Kuluçka Merkezi’nde"
image: "/assets/img/blog/dentiassist-acibadem-kulucka-merkezinde/about-us.jpg"
date:   2021-10-08
author: Dr. Caner Özcan
author_image: "/assets/img/teams/caner.jpg"
author_job: Kurucu, Genel Müdür
tags: [academy,kulucka, acibadem, incubation, dentiassist,deep learning, congress, healthcare]

references:
---

TÜBİTAK BİGG 1512 Girişimcilik Projesi kapsamında desteklenen projemiz **“DentiAssist”**, Acıbadem Üniversitesi Kuluçka Merkezi ve Vodafone Business’ın yürüttüğü **“Dijital Sağlık”** temalı çağrıya yapmış olduğumuz başvuru ön değerlendirmeden başarılı bir şekilde geçerek merkez programına dahil oldu.

<img src="../assets/img/blog/dentiassist-acibadem-kulucka-merkezinde/about-us.jpg" alt="drawing" style="max-height: 100%" />

Program kapsamında sağlık alanında yeni iş fikirlerinin olgunlaştırılması, ürüne dönüştürülüp hayata geçirilmesi noktasında destek vermekte olan Acıbadem Üniversitesi Kuluçka Merkezi planlanan yeni çağrı döneminde dünyanın en büyük teknoloji ve telekom sağlayıcı şirketlerinden biri olan Vodafone Business desteğiyle girişimcileri merkeze kabul etmiştir. Merkeze kabul edilecek tüm girişimciler Acıbadem Üniversitesi’nin teknolojik alt yapısından ve laboratuvarlarından yararlanırken, sektörel çalışma ağına da erişim sağlayabilecektir.


