---
layout: post
title: "Gönüllü Yapay Zeka Stajyeri"
image: "/assets/img/blog/ilan/main.jpg"
date:   2021-02-17
author: Dr. Caner Özcan
author_image: "/assets/img/teams/caner.jpg"
author_job: Kurucu, Genel Müdür
tags: [hiring]

references:
---

![İş İlanı](../assets/img/blog/ilan/yapay-zeka.jpg)

## Genel Nitelikler ve İş Tanımı

Yapay zeka projelerinin geliştirilmesinde ve süreç iyileştirilmesinde yardımcı olabilecek, genç ve dinamik AR-GE ekibimize katılmak isteyen “Gönüllü Stajyer” pozisyonunda yer alacak takım arkadaşları aramaktayız.

### Sizden ne bekliyoruz?

- Makine Öğrenimi ve Derin Öğrenme konuları hakkında bilgi sahibi olup aktif bir şekilde kullanabilmek,
- Derin öğrenme ağlarını projelere uyarlamak
- Yürütülen makine öğrenmesi ve derin öğrenme çözümlerinin canlıya alınması ve test edilmesi sürecinde rol almak,
- Süreçlerin iyileştirmesi için akademik gelişmeleri düzenli takip etmek, proje ve aksiyonlar önermek,
- Tübitak projelerine teknik katkı sağlamak,
- Akademik yayın çalışmalarında bulunmak

### Temel Nitelikler

- Bilgisayar, Elektrik-Elektronik ve Endüstri Mühendisli bölümlerinden birinde eğitimine devam eden,
- Python programlama diline hakim,
- Makine Öğrenimi ve Derin öğrenme tekniklerini kullanmış (Veri hazırlama, sınıflandırma, model eğitimi ve test etme süreçlerinde tecrübeli),
- Tensorflow, Keras veya Pytorch kütüphanelerinden en az birini kullanabilen,
- Teknik doküman anlayabilecek seviyede İngilizce bilgisi olan,
- Algoritma bilgisine sahip,
- Kod analizi yapabilme yeteneğine sahip olan

**Çalışma Şekli:** Pandemi süresince uzaktan çalışma şeklinde olacaktır.

**Bize Ulaşın:** [info@simurgai.com](mailto:info@simurgai.com)
