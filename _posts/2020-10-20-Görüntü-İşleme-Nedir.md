---
layout: post
title: "Görüntü İşleme Nedir?"
image: "/assets/img/blog/goruntu-isleme-teknolojileri/main.jpg"
date:   2020-10-20
author: Süheda Çilek
author_image: "/assets/img/teams/suheda.jpg"
author_job: Yapay Zeka Geliştirici
tags: [image process, goruntu isleme]

references:
- Gonzalez RC, Woods RE, Eddins SL. Digital Image Processing using Matlab. New Jersey, Prentice Hall, 2003.
- Tzimiropoulos G., Zafeiriou S. ve Pantic M., Principal Component Analysis of Image Gradient Orientations for Face Recognition.
- <a href="https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_feature2d/py_sift_intro/py_sift_intro.html" target="_blank">https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_feature2d/py_sift_intro/py_sift_intro.html</a> adresinden alınmıştır.
- <a href="https://www.i2tutorials.com/what-are-sift-and-surf/" target="_blank">https://www.i2tutorials.com/what-are-sift-and-surf/</a> adresinden alınmıştır.
---

![Görüntü İşleme Aşamaları](../assets/img/blog/goruntu-isleme-teknolojileri/1.png)

Görüntü işleme, her biri belirli bir konuma ve değere sahip olan adına resim öğeleri, görüntü öğeleri veya piksel de dediğimiz öğelerin sınırlı sayıda bir araya gelmesiyle oluşan dijital görüntünün bazı bilgisayar algoritmaları ve görsel teknikler kullanılarak amaca uygun hale getirme yöntemine denir. Görüntü bu bilgisayar algoritmaları ile bir fonksiyona atanır ve işlemler bu fonksiyon üzerinden yapılır. Bir görüntü, iki boyutlu bir fonksiyon f(x, y) olarak tanımlanabilir. (x,y) görüntünün o noktadaki yoğunluğu veya gri seviyesi olarak adlandırılır.

### Sayısal Görüntü İşleme Ne demektir?

Sayısal Görüntü İşleme, sayısal görüntüleri bilgisayar aracılığıyla işler. Görüntü işlemeden bilgisayar ile görmeye kadarki alanı düşük, orta ve yüksek diye üç seviyeye ayırabiliriz.


![Sayısal Görüntü İşleme Ne demektir?](../assets/img/blog/goruntu-isleme-teknolojileri/2.png)

**alçak-seviye:** girdileri ve çıktıları olan görüntülerdir.
**orta-seviye:** girişleri genellikle görüntü fakat çıkışları bunlardan oluşturulan nesnelerdir.
**yüksek-seviye:** nesneler topluluğuna görme ile ilgili bilişsel fonksiyonları uygular.

### Sayısal Görüntü İşlemenin Başlangıcı

Dijital görüntünün ilk uygulamalarından biri olan, bir gazete endüstrisinin resimleri ilk kez Londra ve New York arasında denizaltı kablosuyla gönderilmesidir. 1920 lerin başında Bartlane kablolu görüntü aktarım sisteminin getirilmesiyle, bir resmi Atlantik boyunca taşımak için gereken süreyi bir haftadan fazla bir süreden üç saatin altına düşürdü.


![](../assets/img/blog/goruntu-isleme-teknolojileri/3.jpg)
![](../assets/img/blog/goruntu-isleme-teknolojileri/4.jpg)
![](../assets/img/blog/goruntu-isleme-teknolojileri/5.jpg)
![](../assets/img/blog/goruntu-isleme-teknolojileri/6.jpg)

Görüntü işlemenin tarihi hakkında genel bir bilgiye sahip olduktan sonra gelin bir de görüntü işlemenin amaçları ve kullanım alanları hakkında devam edelim. 

![](../assets/img/blog/goruntu-isleme-teknolojileri/7.jpg)

Sayısal görüntü işleme iki ana iş üzerinde odaklanmaktadır:
- İnsanların algılaması ve yorumlaması için resim üzerindeki bilginin iyileştirilmesi.Örnek: Karanlık bir resimdeki detayların histogram eşitleme ile netleştirilmesi.
- Depolama, iletim ve makineler iyi algılasın diye görüntü verisinin işlenmesi. Örnek: Bir kişiyi yüzünden tanıma.
![](../assets/img/blog/goruntu-isleme-teknolojileri/8.jpg)

### Görüntü İşlemenin Kullanım Alanları
![](../assets/img/blog/goruntu-isleme-teknolojileri/9.jpg)

- Yapay Sinir Ağları
- Dalgacık Dönüşümü
- Markov Rasgele Alan Süzgeçleri
- Tümör, damar gibi yapıların belirginlestirilmesi, Tomografi, Ultrason
- Yönlendirme Süzgeçleri
- Genetik Algoritma
- Görüntü İletimi
- Bulanık Mantık
- Yüz tanıma ve Güvenlik Sistemleri
- Uydu görüntüleri üzerinde hava gözlem ve tahmin uygulamaları
- Sualtı uydu görüntülerinin anlamlandırılması ve iyileştirilmesi


![](../assets/img/blog/goruntu-isleme-teknolojileri/10.jpg)

![](../assets/img/blog/goruntu-isleme-teknolojileri/11.jpg)

### Görüntü Kaynakları

![](../assets/img/blog/goruntu-isleme-teknolojileri/12.jpg)


- Elektromanyetik (EM) enerji spektrumu
    - **Gamma-ışını:** Nükleer tıp ve astronomik gözlemler 
    - ** X-ışını:** Medikal teşhis, sanayi ve astronomi, vb.
    - **Mor Ötesi Bandı:** Litografi, endüstriyel denetim, mikroskopi, lazerler, biyolojik görüntüleme ve astronomik gözlemler
    - **Görünür ve Kızılötesi Bantlar:** Işık mikroskopi astronomi, uzaktan algılama, sanayi ve emniyet
    - **Mikrodalga Bandı:** Radar 
    - **Radyo Bandı:** Tıp (MRI gibi) ve astronomi
- Akustik 
- Ultrasonik 
- Elektronik 
- Bilgisayarlar tarafından üretilen sentetik görüntüler

Gelelim görüntü işlemenin adımlarına:
![](../assets/img/blog/goruntu-isleme-teknolojileri/13.jpg)

- **Elde Etme;** Görüntü sayısal kamera ile elde edilir.
- **Görüntü Zenginleştirme, Görüntü Restorasyonu;** adımlarının bütünü ön işleme olarak adlandırılır ve bu adımlar sonucunda görüntü eski halinden daha iyi uygun hale getirilir, görünüm iyileştirilir.
- **Morfolojik İşleme;** Görüntü bileşenlerini ayıklama.
- **Bölütleme:** Sıklıkla Segmentasyon olarak da bahsedilen bölütleme adımında görüntü ya da nesne kendini oluşturan parçacıklara ayırılır.
- **Nesne Tanıma;** tespit ettiğimiz görüntüden çıkarım yapabilmek, tanımak yani bu görüntünün ne olduğunu anlama aşaması olarak basitçe tanımlanabilir.
- **Temsil Etmek ve Açıklamak;** Görüntüyü bilgisayar eşleme için sunma.
- **Görüntü Sıkıştırma;** mümkün olduğunca dosya boyutunu azaltarak kayıt ortamlarında daha az yer kaplamalarını sağlamak.
- **Renkli Görüntü İşleme;**  3 boyutlu matristen oluşan RGB değerlerine sahip görüntüdür.

### GÖRÜNTÜ İŞLEME METOTLARI

Görüntü işleme teknikleri görüntünün nasıl elde edildiği, renk skalası, tekniğin kullanım amacı gibi bazı özelliklere bağlı olarak değişmektedir. 2 boyutlu görüntüler için (x,y) koordinat sisteminde çalışılırken 3 boyutlu görüntüler için (x,y,z) koordinat sisteminde çalışılmaktadır. Örneğin, renkli görüntülerde renk kanalları için geliştirilen teknikler kullanılırken (CCM - Color Co-Occurrence Matrix), gri seviye görüntüler için  daha başka teknikler kullanmak gerekir (GLCM – Gray Level Co-Occurrence Matrix).

![](../assets/img/blog/goruntu-isleme-teknolojileri/14.jpg)

#### Görüntü İşleme Algoritmaları

##### HOG (Histogram of Oriented Gradients)

Yönlendirilmiş gradyanların histogramı (HOG), nesne algılama amacıyla bilgisayarla görme ve görüntü işlemede kullanılan bir özellik tamamlayıcısıdır. Teknik, bir görüntünün lokalize kısımlarında gradyan oryantasyonunun oluşumlarını sayar. Bu yöntem, kenar oryantasyon histogramlarına, ölçekle değişmeyen özellik dönüşümü tanımlayıcılarına ve şekil bağlamlarına benzer, ancak eşit aralıklı hücrelerden oluşan yoğun bir ızgarada hesaplanması ve gelişmiş doğruluk için örtüşen yerel kontrast normalizasyonu kullanması bakımından farklılık gösterir.

![](../assets/img/blog/goruntu-isleme-teknolojileri/15.jpg)

##### GLCM (Gray Level Co-Occurrence Matrix)

Piksellerin uzamsal ilişkisini göz önünde bulunduran dokuyu incelemenin istatistiksel bir yöntemi, gri düzey uzamsal bağımlılık matrisi olarak da bilinen gri düzey eş oluşum matrisidir (GLCM). GLCM işlevleri, bir görüntüde belirli değerlere ve belirli bir uzamsal ilişkiye sahip piksel çiftlerinin ne sıklıkla oluştuğunu hesaplayarak, bir GLCM oluşturarak ve ardından bu matristen istatistiksel ölçüleri çıkararak görüntünün dokusunu karakterize eder.

GLCM algoritması bize birçok öznitelik verir. Bunlardan en yaygın olarak kullanılan 4 öznitelik tablodaki gibidir:

![](../assets/img/blog/goruntu-isleme-teknolojileri/19.jpg)

##### CCM (Color Co-Occurrence Matrix)

Birlikte oluşum matrisi veya birlikte oluş dağılımı, bir görüntü üzerinde birlikte oluşan piksel değerlerinin belirli bir ofsette renk dağılımı olarak tanımlanan bir matristir. Özellikle tıbbi görüntü analizinde çeşitli uygulamalarla doku analizine yaklaşım olarak kullanılmaktadır. RGB görüntüler için her bir renk kanalının (red, green, blue) matris olarak ifade edilmesi ile oluşan 3 boyutlu dizilimin komşuluk değerleri Color Co-Occurrence Matrix algoritması ile öznitelik çıkarımında çok önemlidir. Renk dağılımı ve yoğunluk bilgilerinin detaylı verildiği öznitelik vektörünü ile büyük veriden renk bazında anlamlı veriler çıkarılabilir ve makine öğrenmesi yöntemleri ile veriler sınıflandırılabilir.

![](../assets/img/blog/goruntu-isleme-teknolojileri/16.jpg)

##### PCA (Principal Components Analysis)

PCA, en çok kullanılan boyut azaltma tekniğinden biridir. PCA' yı kullanırken, orijinal verilerimizi girdi olarak alırız ve orijinal veri dağılımını en iyi şekilde özetlenebilecek girdi özelliklerinin bir kombinasyonunu bulmaya çalışırız, böylece orijinal boyutlarını küçültebiliriz. PCA, çift yönlü mesafelere bakarak varyansları en üst düzeye çıkararak ve yeniden yapılandırma hatasını en aza indirerek bunu yapabilir. PCA' da, orijinal verilerimiz bir dizi ortogonal eksene yansıtılır ve her bir eksen önem sırasına göre sıralanır.

![](../assets/img/blog/goruntu-isleme-teknolojileri/17.jpg)

##### SIFT (Scale Invariant Feature Transform)

Bazı görüntüler dönüşle değişmezler, yani görüntü döndürülse bile aynı köşeleri bulabiliriz. Açıktır çünkü döndürülen görüntüde köşeler de köşe olarak kalır. Peki ya ölçeklendirme? Görüntü ölçeklenir ise köşe köşe olmayabilir. Örneğin, aşağıdaki basit bir resme bakın. Küçük bir pencere içindeki küçük bir görüntüdeki köşe, aynı pencerede yaklaştırıldığında düzdür. Yani Harris köşesi ölçekle değişmez değildir.

![](../assets/img/blog/goruntu-isleme-teknolojileri/18.jpg)

Bu yüzden, 2004 yılında, British Columbia Üniversitesi'nden D. Lowe, yeni bir algoritma olan Scale Invariant Feature Transform (SIFT) makalesinde, anahtar noktaları çıkaran ve tanımlayıcılarını hesaplayan Ölçek Değişmez Anahtar Noktalarından Ayırt Edici Görüntü Özellikleri buldu. SIFT özelliği, ilişkili bir tanımlayıcıya sahip seçili bir görüntü bölgesidir (anahtar nokta olarak da adlandırılır). Anahtar noktalar SIFT detektörü tarafından çıkarılır ve bunların tanımlayıcıları SIFT tanımlayıcısı tarafından hesaplanır. Bağımsız olarak SIFT detektörünü (yani anahtar noktaları tanımlayıcılar olmadan hesaplamak) veya SIFT tanımlayıcısını (yani özel anahtar noktalarının hesaplama tanımlayıcılarını) kullanmak yaygındır.

![](../assets/img/blog/goruntu-isleme-teknolojileri/20.jpg)

##### SURF (Speeded-up Robust Features)

Bilgisayarla görmede, hızlandırılmış sağlam özellikler (SURF), patentli bir yerel özellik algılayıcı ve tanımlayıcıdır. Nesne tanıma, görüntü kaydı, sınıflandırma veya 3B yeniden yapılandırma gibi görevler için kullanılabilir. Kısmen ölçekle değişmeyen özellik dönüşümü (SIFT) tanımlayıcısından esinlenmiştir. Standart SURF sürümü, SIFT' den birkaç kat daha hızlıdır ve yazarları tarafından SIFT' den farklı görüntü dönüşümlerine karşı daha sağlam olduğu iddia edilmiştir.

SURF tanımlayıcıları, nesneleri, insanları veya yüzleri bulmak ve tanımak, 3B sahneleri yeniden oluşturmak, nesneleri izlemek ve ilgi çekici noktaları çıkarmak için kullanılmıştır. SURF, SIFT'in hızlandırılmış sürümüdür. SURF biraz daha ileri gider ve LoG'ye Kutu Filtresi ile yaklaşır. Bu yaklaşımın büyük bir avantajı, kutu filtreli evrişimin integral görüntüler yardımıyla kolayca hesaplanabilmesidir. Ve farklı ölçekteki görüntüler de paralel olarak aynı mantıkla hesaplanabilir. Ayrıca, SURF hem ölçek hem de konum için Hessian matrisinin belirleyicisine güvenir. Oryantasyon ataması için SURF, 6x6 boyutundaki bir matris için yatay ve dikey yönde dalgacık yanıtlarını kullanır. Yeterli Guassian ağırlıkları da ona uygulanır. Baskın yönelim, 60 derecelik bir kayan yönelim penceresindeki tüm tepkilerin toplamı hesaplanarak tahmin edilir. Dalgacık tepkisi, herhangi bir ölçekte çok kolay bir şekilde integral görüntüler kullanılarak bulunabilir. SURF, Upright-SURF veya U-SURF adı verilen bir işlevsellik sağlar. Hızı artırır ve dayanıklıdır. OpenCV, bayrağa bağlı olarak her ikisini de dikey olarak destekler. 0 ise yön hesaplanır. 1 ise yönlendirme hesaplanmaz ve daha hızlıdır.

Umarım bu yazı ile en yaygın bilinen görüntü işleme algoritmalarının mantığı daha anlaşılır ifade edilmiştir. Bir sonraki yazıda görüşmek dileğiyle, hoşçakalın!